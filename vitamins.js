const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];

// items that are available 

let availableItems = items.filter((item) => {
    if (item.available) {
        return true;
    }
});

console.log(availableItems);

// items containing only Vitamin C

let fruitWithOnlyVitaminC = items.filter((item) => {
    if (item.contains === 'Vitamin C') {
        return true;
    }
});

console.log(fruitWithOnlyVitaminC);

// items containing Vitamin A

let itemsWithVitaminA = items.filter((item) => {
    if (item.contains.includes("Vitamin A")) {
        return true;
    }
});

console.log(itemsWithVitaminA);

// Sort items based on number of Vitamins

let sortedData = items.sort((first, second) => {
    let numberOfVitaminsInFirst = first.contains.split(',').length;
    let numberOfVitaminsInSecond = second.contains.split(',').length;
    if (numberOfVitaminsInFirst > numberOfVitaminsInSecond) {
        return -1;
    } else {
        return 1
    }
});

console.log(sortedData);

// Group items based on the Vitamins

let groupedItems = items.reduce((accumulator, current) => {
    current.contains.split(', ').map((item) => {
        if (accumulator[item]) {
            accumulator[item] = [accumulator[item] + ", " + current.name];
        } else {
            accumulator[item] = [current.name];
        }
    });
    return accumulator;
}, {});

console.log(groupedItems);

